import sys

import matplotlib.pyplot
import numpy
from simulator import Simulator

def main():
    #############
    # MR Signal #
    #############
    
    # Create the sample
    M0 = numpy.concatenate([7*[0.5], 10*[1], 14*[1]]) # Arbitrary units
    T1 = numpy.concatenate([7*[1], 10*[1], 14*[2]]) # s
    T2 = numpy.concatenate([7*[0.1], 10*[0.1], 14*[2]]) # s
    positions = numpy.linspace(-15e-3, 15e-3, 31) # m
    
    plot_sample(M0, T1, T2, positions)
    
    # Plot the equilibrium magnetization
    simulator = Simulator(M0, T1, T2, positions)
    plot_magnetization(simulator, "M0.png")
    
    # Apply the RF pulse
    flip_angle = numpy.pi/2 # rad
    pulse_duration = 1e-3 # s
    B1 = flip_angle/(simulator.gamma*pulse_duration)
    print("B1 amplitude is {:.1f} µT".format(B1*1e6)) # 5.9 µT
    
    simulator.pulse(flip_angle)
    plot_magnetization(simulator, "M_excited.png")
    
    # Simulate relaxation during 1 s
    duration, step = 1, 40e-3 # s
    signal = numpy.zeros((int(duration/step), len(simulator.positions)))
    for i in range(len(signal)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        signal[i] = numpy.abs(M)
        simulator.idle(step)
    
    plot_evolution(
        numpy.linspace(0, duration, len(signal)), signal[:, (5, 15, 25)].T,
        [
            "Low $M_0$, fast relaxation", "High $M_0$, fast relaxation",
            "High $M_0$, slow relaxation"],
        "Signal (a.u.)", "M_relax.png")
    
    # Signal recording
    TE = 40e-3 # s
    readout_duration = 3e-3 # s
    step = 50e-6 # s
    
    simulator = Simulator(M0, T1, T2, positions)
    simulator.pulse(numpy.pi/2)
    simulator.idle(TE-readout_duration/2)
    
    signal = numpy.zeros(int(readout_duration/step))
    for i in range(len(signal)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        signal[i] = numpy.abs(M.sum())
        simulator.idle(step)
    
    times = numpy.linspace(
        TE-readout_duration/2, TE+readout_duration/2, len(signal))
    plot_evolution(
        times, [signal], ["Total signal"],
        "Signal (a.u.)", "signal_without_gradients.png")
    
    # Image reconstruction
    profile = numpy.fft.fftshift(numpy.fft.fft(numpy.fft.ifftshift(signal)))
    figure, plot = matplotlib.pyplot.subplots(layout="tight")
    plot.plot(numpy.abs(profile))
    plot.set(xlabel="", ylabel="", xticks=[], yticks=[])
    figure.savefig("profile_without_gradients.png")
    
    ####################
    # Spatial encoding #
    ####################
    
    # Precession
    G = 1e-3 # T/m
    duration, step = 100e-6, 1e-6 # s
    
    simulator = Simulator(M0, T1, T2, positions)
    simulator.pulse(numpy.pi/2)
    
    phase = numpy.zeros((int(duration/step), len(simulator.positions)))
    for i in range(len(phase)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        phase[i] = numpy.angle(M)
        simulator.gradient(G, step)
    
    indices = (0, 10, 20, -1)
    plot_evolution(
        numpy.linspace(0, duration, len(phase)), phase[:, indices].T,
        ["{:.3} mm ".format(1e3*positions[x]) for x in indices],
        "Phase (rad)", "precession.png")
    
    # Resolution and field-of-view
    resolution = 1e-3 # m
    G = 2*numpy.pi/(simulator.gamma*resolution*readout_duration) # T/m
    print(f"Gradient amplitude: {1e3*G:.2f} mT/m") # 7.83 mT/m
    field_offset = simulator.positions * G # T
    print(
        "Field variation: "
        f"{1e3*(field_offset.max()-field_offset.min()):.2f} mT") # 0.23 mT
    times = numpy.linspace(0, readout_duration, len(signal))
    fov = 2*numpy.pi*len(times)/(simulator.gamma*G*readout_duration)
    print(f"Field of view: {1e3*fov:.0f} mm") # 60 mm
    
    # Spatial encoding
    step = 50e-6 # s
    simulator = Simulator(M0, T1, T2, positions)
    simulator.pulse(numpy.pi/2)
    simulator.idle(TE-readout_duration)
    simulator.gradient(-G, readout_duration/2)
    
    signal = numpy.zeros(int(readout_duration/step), complex)
    for i in range(len(signal)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        signal[i] = M.sum()
        simulator.gradient(G, step)
    
    times = numpy.linspace(
        TE-readout_duration/2, TE+readout_duration/2, len(signal))
    plot_evolution(
        times, [numpy.abs(signal)], ["Total signal"],
        "Signal (a.u.)", "signal_with_gradients.png")
    
    # Image reconstruction
    profile = numpy.fft.fftshift(numpy.fft.fft(numpy.fft.ifftshift(signal)))
    figure, plot = matplotlib.pyplot.subplots(layout="tight")
    plot.plot(numpy.abs(profile))
    plot.set(xlabel="", ylabel="", xticks=[], yticks=[])
    figure.savefig("profile_with_gradients.png")
    
    ###########################################################
    # Simulate the transient magnetization until steady-state #
    ###########################################################
    
    M0 = [1] # Arbitrary units
    T1 = [1] # s
    T2 = [0.1] # s
    positions = [0] # m
    
    flip_angle = numpy.radians(30)
    TE = 7e-3 # s
    TR = 10e-3 # s
    
    repetitions = 150
    M_y = numpy.zeros(1+repetitions)
    M_z = numpy.zeros(1+repetitions)
    
    simulator = Simulator(M0, T1, T2, positions)
    M_y[0] = simulator.magnetization[:, 1]
    M_z[0] = simulator.magnetization[:, 2]
    for repetition in range(repetitions):
        simulator.pulse(flip_angle)
        simulator.idle(TE)
        M_y[1+repetition] = simulator.magnetization[:, 1]
        M_z[1+repetition] = simulator.magnetization[:, 2]
        simulator.idle(TR-TE)
    
    print("Steady-state of M_y:", M_y[-5:]) # [-0.018 -0.018 -0.018 -0.018 -0.018]
    print("Steady-state of M_z:", M_z[-5:]) # [0.006 0.005 0.005 0.005 0.005]
    
    plot_evolution(
        TE * numpy.arange(1+repetitions),
        [M_y, M_z], ["$M_y$", "$M_z$"],
        "Magnetization (a.u.)", "steady_state.png")

def plot_sample(M0, T1, T2, positions):
    figure, plots = matplotlib.pyplot.subplots(
        1, 3, sharex=True, layout="tight", figsize=(9, 3))
    x_axis = 1e3*positions
    plots[0].plot(x_axis, M0)
    plots[1].plot(x_axis, T1)
    plots[2].plot(x_axis, T2)
    labels = ["$M_0$ (a.u.)", "$T_1$ (s)", "$T_2$ (s)"]
    for plot, label in zip(plots, labels):
        plot.set(xlabel="Position (mm)", ylim=0, ylabel=label)
    figure.savefig("sample.png")

def plot_magnetization(simulator, path):
    figure, plot = matplotlib.pyplot.subplots(layout="constrained")
    x_axis = 1e3*simulator.positions
    plot.plot(x_axis, simulator.magnetization[:, 0], "1-", lw=1, label="$M_x$")
    plot.plot(x_axis, simulator.magnetization[:, 1], "+-", lw=1, label="$M_y$")
    plot.plot(x_axis, simulator.magnetization[:, 2], "2-", lw=1, label="$M_z$")
    plot.set(xlabel="Position (mm)", ylabel="Magnetization (a.u.)")
    plot.legend()
    figure.savefig(path)

def plot_evolution(times, signals, labels, ylabel, path):
    t_axis = 1e3*times # ms
    figure, plot = matplotlib.pyplot.subplots(layout="tight")
    for signal, label in zip(signals, labels):
        plot.plot(t_axis, signal, label=label)
    plot.set(xlabel="Time (ms)", ylabel=ylabel)
    plot.legend()
    figure.savefig(path)

if __name__ == "__main__":
    sys.exit(main())
