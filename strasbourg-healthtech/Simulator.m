classdef Simulator < handle
    properties
        gamma

        M0
        T1
        T2
        positions
        delta_omega
    end
    
    properties (SetAccess = private)
        magnetization_
    end

    properties (Dependent)
        magnetization
    end
    
    methods
        function self = Simulator(M0, T1, T2, positions, delta_omega)
            % TODO: check shapes

            self.gamma = 267522187.44;

            self.M0 = M0;
            self.T1 = T1;
            self.T2 = T2;
            self.positions = positions;
            if nargin >= 5
                self.delta_omega = delta_omega;
            else
                self.delta_omega = zeros(size(M0));
            end

            self.magnetization_ = zeros([4, 1, length(M0)]);
            self.magnetization_(3, :, :) = M0;
            self.magnetization_(4, :, :) = 1;
        end
        
        function M = get.magnetization(self)
            M = self.magnetization_(1:3, :, :) ./ self.magnetization_(4:4, :, :);
            M = squeeze(M);
        end

        function pulse(self, angle, phase)
            if nargin <= 2
                phase = 0;
            end

            operator = eye(4);
            operator(1:3, 1:3) = compute_rotation_matrix( ...
                [cos(phase), sin(phase), 0], angle);
            self.magnetization_ = pagemtimes(operator, self.magnetization_);
        end

        function free_precession(self, duration)
            angle = 2*pi*duration*self.delta_omega;
            operator = zeros(4, 4, length(self.M0));
            for i = 1:length(self.M0)
                operator(1:3, 1:3, i) = compute_rotation_matrix( ...
                    [0, 0, 1], angle(i));
                operator(4, 4, i) = 1;
            end

            self.magnetization_ = pagemtimes(operator, self.magnetization_);
        end

        function relaxation(self, duration)
            E1 = exp(-duration ./ self.T1);
            E2 = exp(-duration ./ self.T2);

            operator = zeros(4, 4, length(self.M0));
            operator(1, 1, :) = E2;
            operator(2, 2, :) = E2;
            operator(3, 3, :) = E1;
            operator(4, 4, :) = 1;
            operator(3, 4, :) = self.M0 .* (1-E1);

            self.magnetization_ = pagemtimes(operator, self.magnetization_);
        end

        function idle(self, duration)
            self.relaxation(duration);
            self.free_precession(duration);
        end

        function gradient(self, amplitude, duration)
            self.relaxation(duration);
            
            delta_B = pagemtimes(amplitude, self.positions);
            delta_omega = self.gamma * delta_B;
            angle = duration * delta_omega;
            
            operator = zeros(4, 4, length(self.M0));
            for i = 1:length(self.M0)
                operator(1:3, 1:3, i) = compute_rotation_matrix( ...
                    [0, 0, 1], angle(i));
                operator(4, 4, i) = 1;
            end
            
            self.magnetization_ = pagemtimes(operator, self.magnetization_);
            
            self.free_precession(duration);
        end
    end
end

function R = compute_rotation_matrix(k, theta)
    K = [
        [0, -k(3), k(2)]
        [k(3), 0, -k(1)]
        [-k(2), k(1), 0]];
    R = eye(3) + K * sin(theta) + K * K * (1-cos(theta));
end