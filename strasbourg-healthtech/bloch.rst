Bloch Equation
==============

In a typical MRI experiment, we assume that the total magnetic field $\B(t)$ at time $t$ is the sum of a static field with magnitude $B_0$ (in T) along the longitudinal $z$ axis, and of a time-varying field in the transversal $xy$ plane $\Bi(t)=[B_x(t),\ B_y(t),\ 0]$: that is, $\B(t) = [B_x(t),\ B_y(t),\ B_0]$.

Given a material with gyromagnetic ratio $\gamma$, with relaxation times $T_1$ and $T_2$ (or relaxation rates $R_1$ and $R_2$), and with equilibrium magnetization $M_0$, the motion of the magnetization vector is described by the Bloch equation:

.. math::
    \frac{\d\M(t)}{\dt} = 
        \begin{bmatrix}
            -R_2 & \gamma B_0 & -\gamma B_y(t) \\
            -\gamma B_0 & -R_2 & \gamma B_x(t) \\
            \gamma B_y(t) & -\gamma B_x(t) & -R_1
        \end{bmatrix}
        \M(t)
        + \begin{bmatrix}0 \\ 0 \\ M_0 R_1\end{bmatrix}

It is usually simpler to consider this equation in a frame of reference which rotates around the $z$ axis at a frequency $\omega_0 = \gamma B_0 - \Delta\omega$, chosen to be the reference frequency of the MR scanner. We will also consider that $\Bi(t) = [B_1 \cos ω_0t,\ B_1 \sin ω_0t,\ 0]$. Under these two assumptions, the Bloch equation can be rewritten as:

.. math::
    \frac{\d\M(t)}{\dt} = 
        \begin{bmatrix}
            0 & \Delta\omega & 0 \\
            -\Delta\omega & 0 & \gamma B_1 \\
            0 & -\gamma B_1 & 0
        \end{bmatrix}
        \M(t)
        + \begin{bmatrix}
            -R_2 & 0 & 0 \\
            0 & -R_2 & 0 \\
            0 & 0 & -R_1
        \end{bmatrix}
        \M(t)
        + \begin{bmatrix}0 \\ 0 \\ M_0 R_1\end{bmatrix}

From this form, we can see that in the rotating frame with an harmonic $\Bi$ field, we have terms related to:

- the frequency offset between the reference frequency of the MRI and the Larmor frequency of the material (terms in $\Delta\omega$)
- the amplitude of the $\Bi$ field (terms in $\gamma B_1$)
- the relaxation (terms in $R_1$ and $R_2$)

When the Larmor frequency of the material matches the reference frequency of the scanner, we have $\Delta\omega=0$, and this behavior is called *on-resonance*; when the two frequencies do not match, $\Delta\omega \ne 0$, and this behavior is called *off-resonance*.

Behavior With Only $\Bi$
------------------------

When an RF-pulse is applied during an MR experiment, it is usually on-resonance, and during a time short enough to neglect the relaxation effects.

- Rewrite the Bloch equation for $\Delta\omega=0$, $T_1 \rightarrow \infty$, and $T_2 \rightarrow \infty$.
- Solve the equation and describe the motion of the magnetization for this specific case (hint: drop the $x$ axis then rewrite in the complex $yz$ plane, i.e. $M_{yz} = M_y + i M_z$).

Reminder: given a function $y$ and two constants $a$ and $b$, the solution to the differential equation $\frac{\d y(t)}{\dt} = ay+b$ with given initial value $y(0)$ is $y(t)=\left(y(0)+\frac{b}{a}\right) e^{at}-\frac{b}{a}$.

.. solution::
    
    With a $B_1$ field, on-resonance and without relaxation, letting $\omega_1 = \gamma B_1$, the Bloch equation simplifies to:
    
    .. math::
        
        \frac{\d\M(t)}{\dt} = 
            \begin{bmatrix}
                0 & 0 & 0 \\ 0 & 0 & \omega_1 \\ 0 & -\omega_1 & 0
            \end{bmatrix}
            \M(t)
    
    By dropping the $x$ axis, we get $\frac{\d M_y(t)}{\d t}=ω_1 M_z$ and $\frac{\d M_z(t)}{\d t}=-ω_1 M_y$. By re-writing the magnetization in the complex $yz$ plane, we have $M_{yz}(t) = M_y(t) + i M_z(t)$ and $\frac{\d M_{yz}}{\dt}(t) = \omega_1(M_z(t)-i M_y(t)) = -i\omega_1 M_{yz}(t)$.
    
    The solution to this is the exponential $M_{yz}(t) = e^{-i\omega_1 t}M_{yz}(0)$, showing that the effect of a $B_1$ field, on-resonance and without relaxation, is a rotation in the $yz$ plane, i.e. around the $x$ axis.

Behavior With Only Relaxation
-----------------------------

When the RF pulse is stopped, two phenomena happen simultaneously. We will start by studying the behavior of the system when $B_1 = 0$, on-resonance, with relaxation.

- Rewrite the Bloch equation for $B_1=0$ and $\Delta\omega=0$.
- Solve the equation and describe the motion of the magnetization in this case, in the transversal $xy$ plane and on the longitudinal axis $z$.

.. solution::
    
    With a $B_1=0$, on-resonance, the Bloch equation simplifies to:
    
    .. math::
        \frac{\d\M(t)}{\d t} = 
            \begin{bmatrix}
                -R_2 & 0 & 0 \\
                0 & -R_2 & 0 \\
                0 & 0 & -R_1
            \end{bmatrix}
            \M(t)
            + \begin{bmatrix}0 \\ 0 \\ M_0 R_1\end{bmatrix}
    
    Once again, we get simple exponential solutions. In the transersal $xy$ plane, the magnetization has an exponential decay:
    
    .. math:: M_{x,y}(t) = e^{-t R_2} M_{x,y}(0)
    
    On the longitudinal axis, the magnetization recovers towards $M_0$:
    
    .. math:: M_z(t) = (M_z(0)-M_0) e^{-t R_1}+M_0
    
    or
    
    .. math:: M_z(t) = M_z(0) e^{-t R_1} + M_0(1-e^{-t R_1})

Behavior With Only Off-resonance
--------------------------------

We now study the behavior of the system when $B_1 = 0$, without relaxation, off-resonance.

- Rewrite the Bloch equation for $B_1=0$, $T_1\rightarrow\infty$ and $T_2\rightarrow\infty$.
- Solve the equation and describe the motion of the magnetization in this case (hint: the simplified equation should look familiar).

.. solution::
    
    In this particular case, the Bloch equation simplifies to:
    
    .. math:: 
        
        \frac{\d\M(t)}{\dt} = 
            \begin{bmatrix}
                0 & \Delta\omega & 0 \\
                -\Delta\omega & 0 & 0 \\
                0 & 0 & 0
            \end{bmatrix}
            \M(t)
    
    By applying the same method as in the $\Bi \ne 0$ case in the complex $xy$ plane, we get:
    
    .. math:: M_{xy}(t) = e^{-i\Delta\omega t}M_{xy}(0)
    
    This shows that with no $\Bi$ and no relaxation, off-resonance, the magnetization precesses in the $xy$ plane, around the $z$ axis.
