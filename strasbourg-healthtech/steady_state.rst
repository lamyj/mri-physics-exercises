Steady State
============

In this last part, you will study how the signal reaches a steady state with an SSFP sequence. First, simplify the object to contain only one point:

.. tab:: Python
        
    .. literalinclude:: simulation.py
        :lines: 141-144
        :dedent: 4

.. tab:: MATLAB
    
    .. literalinclude:: simulation.m
        :lines: 125-128
        :language: matlab

For this SSFP experiment, the flip angle is set to 30° with a pulse duration of 100 µs. The echo time and repetition times are respectively 7 ms and 10 ms.

.. tab:: Python
        
    .. literalinclude:: simulation.py
        :lines: 146-148
        :dedent: 4

.. tab:: MATLAB
    
    .. literalinclude:: simulation.m
        :lines: 130-132
        :language: matlab

The simulation is simplified to neglect several factors:
- No relaxation happens during the RF pulse
- Only the central point of the k-space is simulated, no gradients are applied

Explain why we can make these assumptions and still obtain a meaningful simulation.

.. solution::
    With a pulse duration of 100 µs and a $T_2$ of 100 ms, the relative loss of magnetization due to $T_2$ relaxation is less than $10^{-3}$. The modification of magnetization on the $z$ axis due to $T_1$ relaxation is even lower, less than $10^{-4}$.
    
    The central region of the k-space contains most of the information about the overall contrast of the image, while the periphery of the k-space contains information about details and noise. In this experiment, we simulate an homogeneous object, on-resonance, without noise: it is thus sufficient to simulate the central point of the k-space. At this central point, since $k=2\pi/(\gamma \ G \ \tau)$, the total gradient area is null, and we can dispense with simulating gradients.

Steady-state sequences need a number of repetitions before the steady-state is reached. This number varies with experimental conditions, in our case we will simulate 150 repetitions. We will record the *transient* signal, i.e. the evolution of the magnetization during those 150 repetitions, and save the $M_y$ and $M_z$ components.

Simulate the transient magnetization during the repetitions by repetitively

1. Apply the RF pulse
2. Apply the relaxation before the echo
3. Save the echo
4. Apply the relaxation after the echo

Plot the $M_y$ and $M_z$ magnetization, display the last states and discuss the results.

.. solution::
    .. tab:: Python
            
        .. literalinclude:: simulation.py
            :lines: 150-165
            :dedent: 4

    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 134-150
            :language: matlab
    
    .. image:: steady_state.png
    
    Due to cumulative effects of pulses, the $y$ and $z$ magnetizations vary during the first repetitions. Since the phase of the RF pulse is 0, and no dephasing is introduced by gradients or off-resonance effects, the $x$ magnetization remains null throughout the experiment.
    
    The steady-state conditions are met (constant dephasing, constant timings), and a steady-state is reached by both the $y$ and $z$ magnetization. The $y$ magnetization converges to -0.018, and the $z$ magnetization to 0.005. Upon closer inspection, this is not a perfect steady-state, as the magnetization continues to fluctuate slightly: in practice, diffusion and system imperfections will average those effects, leading to a magnetization which is undistinguishable from a steady-state.
