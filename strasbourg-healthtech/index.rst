MRI Simulator
=============

The goal of this mini-project is to use a simple MRI simulator in Python or MATLAB; more precisely, we are going to find simple solutions to the motion of the magnetization vector during an MRI experiment and simulate simple MRI experiments.

So that you can progress evenly, the solutions to each questions are given in the text, but are hidden unless you click on them.

.. solution::
    
    Don't read unless you're stuck!

For obvious pedagogical reasons, you should try to solve each exercise before looking at the solution.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:
   
   Introduction <self>
   bloch
   simulator
   mr_signal
   spatial_encoding
   steady_state
   complete_solution
