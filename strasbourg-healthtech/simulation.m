%% MR Signal

% Create the sample
M0 = [repelem(0.5, 7), repelem(1, 10), repelem(1, 14)]; % Arbitrary units
T1 = [repelem(1, 7), repelem(1, 10), repelem(2, 14)]; % s
T2 = [repelem(0.1, 7), repelem(0.1, 10), repelem(2, 14)]; % s
positions = linspace(-15e-3, 15e-3, 31); % m

figure; plot_sample(M0, T1, T2, positions);

% Plot the equilibrium magnetization
simulator = Simulator(M0, T1, T2, positions);
figure; plot_magnetization(simulator);

% Apply the RF pulse
flip_angle = pi/2; % rad
pulse_duration = 1e-3; % s
B1 = flip_angle/(simulator.gamma*pulse_duration);
fprintf("B1 amplitude is %.1f µT\n", B1*1e6) % 5.9 µT

simulator.pulse(flip_angle);
figure; plot_magnetization(simulator);

% Simulate relaxation during 1 s
duration = 1; % s
step = 40e-3; % s
signal = zeros(round(duration/step), length(simulator.positions));
for i = 1:length(signal)
    M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
    signal(i, :) = abs(M);
    simulator.idle(step);
end

figure;
plot_evolution( ...
    linspace(0, duration, length(signal)), signal(:, [5, 15, 25]), [ ...
        "Low M_0, fast relaxation", "High M_0, fast relaxation", ...
        "High M_0, slow relaxation"], ...
    "Signal (a.u.)");

% Signal recording
TE = 40e-3; % s
readout_duration = 3e-3; % s
step = 50e-6; % s

simulator = Simulator(M0, T1, T2, positions);
simulator.pulse(pi/2);
simulator.idle(TE-readout_duration/2);

signal = zeros(round(readout_duration/step), 1);
for i = 1:length(signal)
    M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
    signal(i, :) = abs(sum(M));
    simulator.idle(step);
end

times = linspace(TE-readout_duration/2, TE+readout_duration/2, length(signal));
figure; plot_evolution(times, signal, ["Total signal"], "Signal (a.u.)");

% Image reconstruction
profile = fftshift(fft(ifftshift(signal)));
figure; plot(abs(profile)); xticks([]); yticks([]);

%% Spatial encoding

% Precession
G = 1e-3; % T/m
duration = 100e-6; % s
step = 1e-6; % s

simulator = Simulator(M0, T1, T2, positions);
simulator.pulse(pi/2);

phase = zeros(round(duration/step), length(simulator.positions));
for i=1:length(phase)
    M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
    phase(i, :) = angle(M);
    simulator.gradient(G, step);
end

indices = [1, 10, 20, size(phase, 2)];
labels = strings(1, length(indices));
for i=1:length(indices)
    labels(i) = sprintf("%.1f mm", 1e3*positions(indices(i)));
end
figure;
plot_evolution( ...
    linspace(0, duration, length(phase)), phase(:, indices), ...
    labels, "Phase (rad)");

% Resolution and field-of-view
resolution = 1e-3; % m
G = 2*pi/(simulator.gamma*resolution*readout_duration); % T/m
fprintf("Gradient amplitude: %.2f mT/m\n", 1e3*G);
field_offset = simulator.positions * G; % T
fprintf(...
    "Field variation: %.2f mT\n", ...
    1e3*(max(field_offset) - min(field_offset))); % 0.23 mT
times = linspace(0, readout_duration, length(signal));
fov = 2*pi*length(times)/(simulator.gamma*G*readout_duration);
fprintf("Field of view: %.0f mm\n", 1e3*fov); % 60 mm

% Spatial encoding
step = 50e-6; % s
simulator = Simulator(M0, T1, T2, positions);
simulator.pulse(pi/2);
simulator.idle(TE-readout_duration);
simulator.gradient(-G, readout_duration/2);

signal = zeros(round(readout_duration/step), 1);
for i=1:length(signal)
    M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
    signal(i) = sum(M);
    simulator.gradient(G, step);
end

times = linspace(TE-readout_duration/2, TE+readout_duration/2, length(signal));
figure; plot_evolution(times, abs(signal), "Total signal", "Signal (a.u.)");

% Image reconstruction
profile = fftshift(fft(ifftshift(signal)));
figure; plot(abs(profile)); xticks([]); yticks([]);

%% Simulate the transient magnetization until steady-state
M0 = [1]; % Arbitrary units
T1 = [1]; % s
T2 = [0.1]; % s
positions = [0]; % m

flip_angle = deg2rad(30);
TE = 7e-3; % s
TR = 10e-3; % s

repetitions = 150;
M_y = zeros(1+repetitions, 1);
M_z = zeros(1+repetitions, 1);

simulator = Simulator(M0, T1, T2, positions);
M_y(1, :) = simulator.magnetization(2, :);
M_z(1, :) = simulator.magnetization(3, :);
for repetition = 1:repetitions
    simulator.pulse(flip_angle);
    simulator.idle(TE);
    M_y(1+repetition, 1) = simulator.magnetization(2, :);
    M_z(1+repetition, 1) = simulator.magnetization(3, :);
    simulator.idle(TR-TE);
end

fprintf("Steady-state of M_y: %s\n", sprintf("%.3f ", M_y(end-4:end))); % [-0.018 -0.018 -0.018 -0.018 -0.018]
fprintf("Steady-state of M_z: %s\n", sprintf("%.3f ", M_z(end-4:end))); % [0.006 0.005 0.005 0.005 0.005]

figure
plot_evolution( ...
    TE*(1:(1+repetitions)), [M_y, M_z], ["M_y", "M_z"], "Magnetization (a.u.)")

%% Utility functions

function plot_sample(M0, T1, T2, positions)
    x_axis = 1e3*positions;
    ys = [M0; T1; T2];
    titles = ["M_0 (a.u.)" "T_1 (s)" "T_2 (s)"];
    for i=1:size(ys, 1)
        subplot(1, 3, i);
        plot(x_axis, ys(i, :));
        xlabel("Position (mm)"); ylabel(titles(i));
        ylim("Padded"); l = ylim; ylim([0 l(2)]);
    end
end

function plot_magnetization(simulator)
    x_axis = 1e3*simulator.positions;
    
    hold on;
    plot(x_axis, simulator.magnetization(1, :), "+");
    plot(x_axis, simulator.magnetization(2, :), "x");
    plot(x_axis, simulator.magnetization(3, :), "square");
    hold off;
    
    ylim([-1.02, 1.02]);
    xlabel("Position (mm)");
    ylabel("Magnetization (a.u.)");
    legend("M_x", "M_y", "M_z")
end

function plot_evolution(times, signals, labels, ylabel_)
    t_axis = 1e3*times; % ms
    
    hold on
    for i = 1:size(signals, 2)
        plot(t_axis, signals(:, i));
    end
    
    hold off
    xlabel("Time (ms)");
    ylabel(ylabel_);
    legend(labels);
end
