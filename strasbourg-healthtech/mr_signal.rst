MR Signal
=========

In this part, you will simulate a simple NMR experiment, i.e. without magnetic field gradients, and perform an "image" reconstruction of the temporal signal.

Description of the Sample
-------------------------

The sample we will simulate a simple one-dimensional structure, comprised of three "objects", whose equilibrium magnetization $M_0$ (in arbitrary units) and relaxation times $T_1$ and $T_2$ are given below.

+--------------------+-------+-----------+-----------+
| Position (cm)      | $M_0$ | $T_1$ (s) | $T_2$ (s) |
+====================+=======+===========+===========+
| [-1.5, -0.8[       | 0.5   | 1         | 0.1       |
+--------------------+-------+-----------+-----------+
| [-0.8, +0.2[       | 1     | 1         | 0.1       |
+--------------------+-------+-----------+-----------+
| [+0.2, +1.5]       | 1     | 2         | 2         |
+--------------------+-------+-----------+-----------+

.. image:: sample.png

.. tab:: Python
    
    .. literalinclude:: simulation.py
        :lines: 13-16
        :dedent: 4

.. tab:: MATLAB
    
    .. literalinclude:: simulation.m
        :lines: 4-7
        :language: matlab

Create a simulator, initialized with the equilibrium magnetization of the sample, and plot the magnetization along the $x$, $y$, and $z$ axes.

.. solution::
  
    .. tab:: Python

        .. literalinclude:: simulation.py
            :lines: 21
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 21
            :language: matlab
    
    .. image:: M0.png

RF Pulse
--------

We wish to move the equilibrium magnetization from the longitudinal axis $z$ to the transverse plane $xy$ so that it can be detected, i.e. apply a 90° RF pulse.

- Assuming a pulse duration of 1 ms, what is the required amplitude of $B_1$?
- Clinical scanners have a static $B_0$ field magnitude of a couple of teslas, and the Earth's magnetic field is around 50 µT. How does the magnitude of $B_1$ you have computed compare to those values?

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 25-28
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 16-19
            :language: matlab
    
    The amplitude of the $B_1$ field required to tip the magnetization by 90° is very small compared to the static $B_0$ field of the scanner (6 orders of magnitude), and small compared to the Earth's magnetic field (one order of magnitude).

Simulate the RF and plot the magnetization to verify that it has indeed been rotated to the $y$ axis. Set the flip angle to 30°, compare and discuss the results

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 30
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 21
            :language: matlab
    
    .. image:: M_excited.png
    
    With a flip angle of 90°, the magnetization has been completely rotated to the $y$ axis. This is because the phase of the RF pulse is implicitely 0°, and the rotation axis is hence $x$.
    
    With a flip angle of 30°, we can observe a partial transfer from the longitudinal axis $z$ to the transversal axis $y$, with respective components equal to the cosine and sine of the flip angle.

Relaxation
----------

We will now study the effects of relaxation for 1 s, every 40 ms. 

- Simulate the evolution of the magnetization at each time step
- Compute the MRI signal at each point, defined as $M_x+iM_y$
- Plot the time course of the magnitude of the signal at one point in each region of the sample (e.g. at positions 5, 15 and 25)
- Discuss the results, with respect to effect on signal recording

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 34-39
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 25-32
            :language: matlab
    
    .. image:: M_relax.png
    
    For the regions with fast relaxation, we can see that the transversal magnetization vanishes at around 5$T_2$ (500 ms), regardless of the starting point: for those regions, no signal will be recorded after that time. For the slow-relaxing region, there is still over 60% of the signal available for recording after the same period.

Signal Recording and Image Reconstruction
-----------------------------------------

The previous result show that the MRI signal should be recorded on a short time frame in order to be mostly unaffected by the relaxation. However, if we record the signal too soon after the RF pulse, there may be a lack of contrast between tissues since the relaxation is the main source of contrast.

We will simulate a readout of 3 ms, with an echo time of 40 ms, at a 50 µs time step.

- As before, simulate the evolution of the magnetization at each time step, but using the readout conditions above.
- Define the *total* MRI signal as the complex sum of the signal at each individual point
- Plot the time course of the magnitude of the total signal
- Discuss the results

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 49-61
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 42-55
            :language: matlab
    
    .. image:: signal_without_gradients.png
    
    Over 3 ms, the total signal decays by less than 2%: this is mainly due to the fast-relaxing regions, and we can consider that this variation is too small to have a large effect on the resulting image.

Recall that an image can be obtained by the Fourier transform of the time-varying MRI signal: compute the "image" that would be acquired using the current simulation and discuss the results. Note that some texts mention an inverse Fourier transform instead of a direct Fourier transform: it is simply a matter of convention regarding the sign of the phase of the signal.

Note that numpy's FFT functions, as in MATLAB, expect the central data not at the center of the array, but at the beginning. To perform a *centered* FFT, use :code:`numpy.fft.fftshift(numpy.fft.fft(numpy.fft.ifftshift(x)))` in Python and :code:`fftshift(fft(ifftshift(x)))` in MATLAB.

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 70
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 61
            :language: matlab
    
    .. image:: profile_without_gradients.png
    
    Since the time signal is mostly constant, its Fourier transform does not include information regarding the position. The single peak we observe is typical in an MR spectroscopy experiment, where the resonant frequencies of the sample, and not its spatial composition, are of interest.
