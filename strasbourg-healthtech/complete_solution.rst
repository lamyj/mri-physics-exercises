Complete solution
=================

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
