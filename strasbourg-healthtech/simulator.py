import matplotlib.pyplot
import numpy

class Simulator(object):
    def __init__(self, M0, T1, T2, positions, delta_omega=None):
        self.gamma = 267522187.44 # rad/s/T
        
        M0, T1, T2, positions = [
            numpy.asarray(x) for x in [M0, T1, T2, positions]]
        
        if delta_omega is None:
            delta_omega = numpy.zeros(M0.shape)
        else:
            delta_omega = numpy.asarray(delta_omega)
        
        if any(M0.shape != x.shape for x in [T1, T2, delta_omega]):
            raise Exception("All inputs must be have the same shape")
        if M0.shape != positions.shape[:1]:
            raise Exception("All inputs must be have the same shape")
        
        self.M0 = M0
        self._magnetization = numpy.zeros((len(M0), 4))
        self._magnetization[:, 2] = M0
        self._magnetization[:, 3] = 1
        
        self.T1 = T1
        self.T2 = T2
        self.positions = (
            positions if positions.ndim != 1 else positions.reshape(-1, 1))
        self.delta_omega = delta_omega
    
    @property
    def magnetization(self):
        # NOTE: self.M[:, 3] will always be one
        return self._magnetization[:,:3] / self._magnetization[:,3:]
    
    def pulse(self, angle, phase=0.):
        operator = numpy.identity(4)
        operator[:3, :3] = compute_rotation_matrix(
            [numpy.cos(phase), numpy.sin(phase), 0], angle)
        
        self._magnetization = numpy.einsum(
            "ij,nj->ni", operator, self._magnetization)
    
    def free_precession(self, duration):
        angle = 2*numpy.pi*duration*self.delta_omega
        
        operator = numpy.zeros((len(self.delta_omega), 4, 4))
        operator[:, :3, :3] = numpy.array(
            [compute_rotation_matrix([0, 0, 1], x) for x in angle])
        operator[:, 3, 3] = 1
        
        self._magnetization = numpy.einsum(
            "nij,nj->ni", operator, self._magnetization)
    
    def relaxation(self, duration):
        E1 = numpy.exp(-duration/self.T1)
        E2 = numpy.exp(-duration/self.T2)
        
        operator = numpy.zeros((len(self.T1), 4, 4))
        operator[:, 0, 0] = E2
        operator[:, 1, 1] = E2
        operator[:, 2, 2] = E1
        operator[:, 3, 3] = 1
        operator[:, 2, 3] = self.M0*(1-E1)
        
        self._magnetization = numpy.einsum(
            "nij,nj->ni", operator, self._magnetization)
    
    def idle(self, duration):
        self.relaxation(duration)
        self.free_precession(duration)
    
    def gradient(self, amplitude, duration):
        self.relaxation(duration)
        
        delta_B = numpy.dot(amplitude, self.positions)
        delta_omega = self.gamma * delta_B
        angle = duration * delta_omega
        
        operator = numpy.zeros((len(delta_omega), 4, 4))
        operator[:, :3, :3] = numpy.array(
            [compute_rotation_matrix([0, 0, 1], x) for x in angle])
        operator[:, 3, 3] = 1
        
        self._magnetization = numpy.einsum(
            "nij,nj->ni", operator, self._magnetization)
            
        self.free_precession(duration)

def compute_rotation_matrix(k, theta):
    """ Return the matrix corresponding to the rotation around
        axis k by an angle theta (in radians).
    """
    
    K = numpy.array([
        [0, -k[2], k[1]],
        [k[2], 0, -k[0]],
        [-k[1], k[0], 0],
    ])
    R = (
        numpy.identity(3)
        + K*numpy.sin(theta)
        + numpy.linalg.matrix_power(K, 2) * (1-numpy.cos(theta)))
    return R
