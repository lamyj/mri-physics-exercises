Spatial Encoding
================

In this part, you will simulate the same experiment as in the previous part, additionally showing how magnetic field gradients modulate the MRI signal to encode spatial information in a temporal signal.

Precession
----------

Up to now, the phase of the magnetization in the transversal plane at each voxel has been constant. You can check this by looking at the $x$ component of the magnetization or at the argument of the complex signal. This coherence across space and time explains why the Fourier transform of the signal lacks any spatial information.

The spatial information of the sample can be encoded in the MR signal by using magnetic field gradients so that the total magnetic field $\B$, and thus the Larmor frequency, depends on the position. In the rotating frame, this will translate to a non-zero phase, which will vary at each voxel and at each time point.

Given a gradient amplitude of 1 mT/m, compute the field offset at each position and the frequency offset and each position. Simulate the phase evolution of the magnetization at each point, right after the pulse, every 1 µs for 100 µs. Plot at -15 mm, -5 mm, 5 mm and 15 mm, and discuss the results.

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 81-91
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 67-79
            :language: matlab
    
    .. image:: precession.png
    
    We can see that for positions on the "left" (i.e. negative), the phase decreases with time, while for positions on the right (i.e. positive), the phase increases with time. This is in line with the frequency offsets. Moreover we can see that the phase derivative depends on the position, with positions closer to the center having a less steep derivative. Once again, this is in line with the simulated gradient, which yields a vanishing field offset at the center.

Resolution and Field-of-view
----------------------------

The amplitude of the gradient will affect the resolution of the image, where higher gradients yield a finer resolution. To attain a resolution of $\Delta x$ m per pixel, the amplitude of the gradient, in T/m is given by $2\pi/(\gamma\ \Delta x\ t)$, where $t$ is the readout duration. The field-of-view of the resulting image can the be derived by $2\pi\ N/(\gamma\ G\ t)$, where $N$ is the number of points sampled.

Given a target definition of 1 mm, a readout duration of 3 ms, and a sample read every 50µs, compute the required gradient magnitude, the magnetic field offset at each position, the frequency offset at each position, and the field of view.

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 100-109
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 92-101
            :language: matlab

Spatial Encoding
----------------

Using gradients, we can express the MRI signal in the k-space, where $k$ is defined as a spatial frequency related to the phase accumulation caused by the gradients: 

$$k(t) = \frac{\gamma}{2\pi}\int_0^t G(\tau)\d\tau$$

Since $k$ depends on the time, recording the signal while applying a gradients captures the signal at a different point in the k-space a each time step.

Recall that the center of the k-space ($k=0$) encodes the contrast of the image, while its borders encode the details and the noise.

Combining these two observations show that in order to acquire the whole k-space, we need to move to the edge of the k-space *before* starting the record the signal: this is done by simply applying a gradient with negative polarity before starting sampling the signal.

Compute the precession during this preparation gradient, combine it with the relaxation matrix defined previously, compute the evolution of the magnetization, and plot it during the course of the signal sampling. Compare with the signal sampled without gradient and discuss.

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 112-122
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 104-115
            :language: matlab
    
    .. image:: signal_with_gradients.png
    
    When magnetic field gradients are applied, a spatial modulation is imparted on the magnitude of the signal, caused by phase variations related to the spatial variation of precession frequency.

Compute the "image" that would be acquired using the current simulation, compare with the results obtained with different echo times (5 ms and 100 ms), and discuss the results.

.. solution::
    .. tab:: Python
        
        .. literalinclude:: simulation.py
            :lines: 131
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: simulation.m
            :lines: 121
            :language: matlab
    
    .. image:: profile_with_gradients.png
    
    Since the gradients encode the spatial position by the precession frequency, the profile show three zones corresponding to the three parts of our object. The contrast varies with the echo time: when sampled earlier, (TE=5 ms), there is a very low $T_2$ contrast since the transversal magnetization depends mostly on $M_0$. At a later echo time (TE=100 ms), this contrast is improved. However, in regions with fast relaxation, the signal decreases with the echo time, due to $T_2$ relaxation.
