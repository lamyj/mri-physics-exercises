# TP "Build Your Own MRI Simulator"

Ce TP a été originalement conçu pour le master Healthtech de l'Université de Strasbourg.

Ce TP est basé sur Python et Sphinx. Pour la simulation à proprement parler, deux paquets sont nécessaires, `numpy` et `matplotlib`. Pour la génération du site web, en plus de Sphinx, quatre paquets sont nécessaires : `sphinxcontrib.katex`, `sphinx_math_dollar`, `sphinx_togglebutton` et `furo`. Les dépendances peuvent être installées par `pip`, en lançant, depuis ce répertoire

```bash
pip install -U -r requirements.txt
```

Le site web du TP est généré, en lançant, depuis ce répertoire

```bash
make all
```

Les fichiers se trouvent alors dans le répertoire `_build/html`, la page principale étant sauvée dans `index.html`.
