Simulator
=========

The simulator used in this project is based on the simple solutions to the Bloch equation from the previous part, and is available in `Python <simulator.py>`_ and `MATLAB <Simulator.m>`_. In MATLAB, no external dependancy is used; in Python, `numpy <https://numpy.org/>`_ and `matplotlib <https://matplotlib.org/>`_ are recommended for this project.

The file with the source code in the language of your choice must be downloaded from one of the previous links and stored somewhere within the search paths of your environment, e.g. in the directory you will use for this project.

The rest of this section describes how to use the simulator.

Creation of a simulation
------------------------

The simulated "sample" is specified through its positions and its basic NMR properties ($M_0$, $T_1$ and $T_2$):

- the positions is an array of spatial coordinates, specified in meters. It can be 1D or more.
- $M_0$ is an array of initial $z$ magnetization, with one value per position.
- $T_1$ and $T_2$ are arrays of relaxation times, expressed in seconds, with one value per position.

Once these arrays are defined, use the following instructions to create a simulation.

.. tab:: Python
    
    .. code:: python
        
        simulator = Simulator(M0, T1, T2, positions)

.. tab:: MATLAB
    
    .. code:: matlab
        
        simulator = Simulator(M0, T1, T2, positions);

The current magnetization is always stored in the ``magnetization`` member:

.. tab:: Python
    
    .. code:: python
        
        # Array of shape N × 3
        simulator.magnetization

.. tab:: MATLAB
    
    .. code:: matlab
        
        % Array of shape N × 3
        simulator.magnetization;


RF Pulses
---------

To simulate the application of an RF pulse, the ``pulse`` function is used: it takes one mandatory parameter, the flip angle (in radians), and one optional parameter, the phase (in radians).

.. tab:: Python
    
    .. code:: python
        
        # 90° pulse with phase 0°
        simulator.pulse(numpy.pi/2)
        # 40° pulse with phase 180°
        simulator.pulse(numpy.pi/4, numpy.pi)

.. tab:: MATLAB
    
    .. code:: matlab
        
        % 90° pulse with phase 0°
        simulator.pulse(pi/2);
        % 40° pulse with phase 180°
        simulator.pulse(pi/4, pi);

Time Intervals
--------------

The ``idle`` function simulates idle time; it takes one parameter: the duration (in seconds). 

.. tab:: Python
    
    .. code:: python
        
        # Idling for 100 ms
        simulator.idle(0.1)

.. tab:: MATLAB
    
    .. code:: matlab
        
        % Idling for 100 ms
        simulator.idle(0.1);

The ``gradient`` function simulates the application of a constant magnetic field gradient; it takes two parameters: the amplitude (in T/m) and the duration (in seconds).

.. tab:: Python
    
    .. code:: python
        
        # Apply a gradient of 20 mT/m during 5 ms
        simulator.gradient(0.02, 5e-3)

.. tab:: MATLAB
    
    .. code:: matlab
        
        % Apply a gradient of 20 mT/m during 5 ms
        simulator.gradient(0.02, 5e-3)
